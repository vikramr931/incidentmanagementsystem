package com.IncidentManagement.System.IMS.imsService;

import com.IncidentManagement.System.IMS.imsModel.Incident;

import java.util.List;

public interface IncidentService {

    Incident addIncident(String userName, String password,Incident incident);

    List<Incident> getAllIncidentOfUser(String userName, String password);

    Incident getIncidentById(String userName,String password, Long id);

    Incident updateIncidentById(String userName,String password, Long id, Incident incident);
}
