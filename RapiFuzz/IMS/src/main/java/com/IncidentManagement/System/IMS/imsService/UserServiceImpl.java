package com.IncidentManagement.System.IMS.imsService;

import com.IncidentManagement.System.IMS.imsModel.User;
import com.IncidentManagement.System.IMS.imsRepository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    UserServiceImpl(UserRepository userRepository){
        this.userRepository=userRepository;
    }
    @Override
    public User createUser(User user) {

        return userRepository.save(user);
    }

    @Override
    public Optional<User> getUserByUserName(String userName) {
       Optional<User> users= userRepository.findByUsername(userName);
        return users;
    }
}
