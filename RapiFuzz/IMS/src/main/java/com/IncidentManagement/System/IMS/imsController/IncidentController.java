package com.IncidentManagement.System.IMS.imsController;

import com.IncidentManagement.System.IMS.imsModel.Incident;
import com.IncidentManagement.System.IMS.imsService.IncidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/incidents")
public class IncidentController {


    @Autowired
    IncidentService incidentService;

    @PostMapping("/{userName}/{password}")
    public Incident addIncident( @PathVariable String userName,@PathVariable String password,@RequestBody Incident incident){
        return incidentService.addIncident(userName, password, incident);

    }

    @GetMapping("/{userName}/{password}")
    public List<Incident> getAllIncidents(@PathVariable String userName, @PathVariable String password){
       return incidentService.getAllIncidentOfUser(userName, password);

    }

    @GetMapping("/{userName}/{password}/{id}")
    public Incident getIncidentById(@PathVariable String userName, @PathVariable String password,@PathVariable Long id){
       return incidentService
                .getIncidentById(userName, password, id);
    }

    @PutMapping("/{userName}/{password}/{id}")
    public Incident updateIncidentById(@PathVariable String userName, @PathVariable String password,@PathVariable Long id,@RequestBody Incident incident){
        return incidentService.updateIncidentById(userName, password, id, incident);
    }


}
