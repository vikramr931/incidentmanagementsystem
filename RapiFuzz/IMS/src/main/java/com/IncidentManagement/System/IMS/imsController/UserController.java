package com.IncidentManagement.System.IMS.imsController;

import com.IncidentManagement.System.IMS.imsModel.User;
import com.IncidentManagement.System.IMS.imsService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user){

        return new ResponseEntity<>(userService.createUser(user),HttpStatus.CREATED);
    }


    //@GetMapping
//    public ResponseEntity<Optional<User>> getUserByUserName(@PathVariable String userName,psss){
//        //get user by username if exist put u if not throw err
//        //u.pass=check pass
//        get
//        return ResponseEntity.ok(userService.getUserByUserName(userName));
//    }
}
