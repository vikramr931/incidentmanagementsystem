package com.IncidentManagement.System.IMS.ExceptionHandle;

public class UserNotFoundException extends Exception{
    UserNotFoundException(String s){
        super(s);
    }
}
