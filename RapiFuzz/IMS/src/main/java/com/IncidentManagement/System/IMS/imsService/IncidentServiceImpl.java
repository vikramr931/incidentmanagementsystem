package com.IncidentManagement.System.IMS.imsService;

import com.IncidentManagement.System.IMS.imsModel.Incident;
import com.IncidentManagement.System.IMS.imsModel.User;
import com.IncidentManagement.System.IMS.imsRepository.IncidentRepository;
import com.IncidentManagement.System.IMS.imsRepository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.Random;


@Service
public class IncidentServiceImpl implements IncidentService{

    IncidentRepository incidentRepository;
    UserRepository userRepository;


    public static String generateIncidentId() {
        Random random = new Random();
        int randomNumber = random.nextInt(90000) + 10000; // Generate a random 5-digit number
        int currentYear = Year.now().getValue(); // Get the current year
        return "RMG" + randomNumber + currentYear;
    }

    IncidentServiceImpl(IncidentRepository incidentRepository,UserRepository userRepository){
        this.incidentRepository=incidentRepository;
        this.userRepository=userRepository;

    }

    public Incident addIncident(String userName, String password,Incident incident){
        User u=userRepository.findByUsername(userName).orElseThrow(()-> new RuntimeException());
        incident.setIncidentId(generateIncidentId());
        if(password.equals(u.getPassword()) && userName.equals(u.getUsername())) {
            return incidentRepository.save(incident);
        }
        return null;
    }
    @Override
    public List<Incident> getAllIncidentOfUser(String userName, String password) {
        List<Incident> incidents=null;
        User u=userRepository.findByUsername(userName).orElseThrow(()-> new RuntimeException("not found"));
        if(password.equals(u.getPassword()) && userName.equals(u.getUsername())){
            incidents=  incidentRepository.findByReporterName(userName);
          return incidents;
        }

        return null;
    }

    @Override
    public Incident getIncidentById(String userName, String password, Long id) {
        User u=userRepository.findByUsername(userName).orElseThrow(()-> new RuntimeException("not found"));
        if(password.equals(u.getPassword()) && userName.equals(u.getUsername())){
          Incident incident= incidentRepository.findById(id).orElseThrow(()->new RuntimeException("incident not found with id"+id));
           return incident;
        }

        return null;
    }

    @Override
    public Incident updateIncidentById(String userName, String password, Long id, Incident incident) {
        User u=userRepository.findByUsername(userName).orElseThrow(()-> new RuntimeException("not found"));
        if(password.equals(u.getPassword()) && userName.equals(u.getUsername())){
            incident.setIncidentId(generateIncidentId());
           Incident incident1= incidentRepository.findById(id).orElseThrow(()->new RuntimeException("not found with id"+id));
           incident1.setIncidentId(incident.getIncidentId());
           incident1.setId(incident.getId());
           incident1.setIncidentDetails(incident.getIncidentDetails());
           incident1.setPriority(incident.getPriority());
           incident1.setStatus(incident.getStatus());
           incident1.setEnterpriseOrGovernment(incident.getEnterpriseOrGovernment());
           incident1.setReportedDateTime(incident.getReportedDateTime());

          incidentRepository.save(incident1);

            return incident1;
        }

        return null;

    }
}
