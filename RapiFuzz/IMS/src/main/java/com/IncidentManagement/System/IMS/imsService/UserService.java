package com.IncidentManagement.System.IMS.imsService;

import com.IncidentManagement.System.IMS.imsModel.User;

import java.util.Optional;

public interface UserService {
    User createUser(User user);

    Optional<User> getUserByUserName(String userName);


}
