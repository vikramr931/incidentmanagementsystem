package com.IncidentManagement.System.IMS.imsRepository;

import com.IncidentManagement.System.IMS.imsModel.Incident;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IncidentRepository extends JpaRepository<Incident, Long> {

//   Incident findByIncidentId(String incidentId);
    List<Incident> findByReporterName(String reporterName);
}
